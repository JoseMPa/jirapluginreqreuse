package com.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.bc.issue.search.SearchService;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.query.Query;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.web.bean.PagerFilter;

import com.atlassian.jira.security.JiraAuthenticationContext;

import com.atlassian.jira.component.ComponentAccessor;

import static com.google.common.base.Preconditions.checkNotNull;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueInputParameters;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;

import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.Field;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.LinkCollection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;



public class ReqReuseServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(ReqReuseServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        
        // Recuperar datos a través de los parametros de la url
        String keyIssue = req.getParameter("key");
        String proyectoReutilizar = req.getParameter("proyectoR");

        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getComponent(JiraAuthenticationContext.class);
        IssueService issueService = ComponentAccessor.getComponent(IssueService.class);
        IssueFactory issueFactory  = ComponentAccessor.getComponent(IssueFactory.class);
        IssueManager issueManager   = ComponentAccessor.getComponent(IssueManager.class);
        ProjectManager projectManager   = ComponentAccessor.getComponent(ProjectManager.class);
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        FieldManager fieldManager = ComponentAccessor.getComponent(FieldManager.class);

        Field enlaces = fieldManager.getField(IssueFieldConstants.ISSUE_LINKS);



        ApplicationUser user = authenticationContext.getLoggedInUser();

        String usuario = user.getUsername();

        JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();

        // Consulta que devuelve la issue con keyIssue
        Query query = jqlClauseBuilder.issue(keyIssue).buildQuery();

        JqlClauseBuilder jqlClauseBuilder2 = JqlQueryBuilder.newClauseBuilder();

        // Comprobación de que el campo "proyectoReutilizar" tenga un valor (Cuando no tiene un valor al obtener el campo es "$proyectoReutilizar")
        if(proyectoReutilizar.equals("$proyectoReutilizar")){
            resp.setContentType("text/html");
            resp.getWriter().write("<html><body>Debe indicar un proyecto a donde se desee reutilizar el requisito</body></html>");
            return;
        }
        
        // Consulta que devuelve todas la issue del proyectoReutilizar
        Query query2 = jqlClauseBuilder2.project(proyectoReutilizar).buildQuery();


        PagerFilter pagerFilter2 = PagerFilter.getUnlimitedFilter();

        SearchResults searchResults2 = null;
        
        try {
            searchResults2 = searchService.search(user, query2, pagerFilter2);

        } catch (SearchException e) {
            System.out.println("Error: " + e.getMessage());
        }

        List<Issue> issues2 = searchResults2.getIssues();

        List<String> nombresRequisitos= new ArrayList<String>();

        Map<String, Long> nombresIds = new HashMap<String, Long>();

        // Rellenar map asociando el nombre de la issue con su id
        if(issues2.size() != 0){
            for(Issue iss : issues2){
               nombresRequisitos.add(iss.getSummary());
               nombresIds.put(iss.getSummary(), iss.getId());
            }
            
        }

        //
        

        PagerFilter pagerFilter = PagerFilter.getUnlimitedFilter();

        SearchResults searchResults = null;
        
        try {
            searchResults = searchService.search(user, query, pagerFilter);

        } catch (SearchException e) {
            System.out.println("Error: " + e.getMessage());
        }

        List<Issue> issues = searchResults.getIssues();
        Long projectId = null;
        String type = "";
        Issue i = null;


        ArrayList<Issue> links = null;
        if(issues.size() != 0){
            i = issues.get(0);
            projectId = i.getProjectId();
            type = i.getIssueType().getName();

            // Recuperar los enlaces
            if(enlaces != null){
                LinkCollection linkCollection = issueLinkManager.getLinkCollection(i, user);
                Collection<Issue> clinks = linkCollection.getAllIssues();

                Issue cloned = null;                
                Project proyectoR = projectManager.getProjectByCurrentKey(proyectoReutilizar);

                boolean existeOriginal = nombresRequisitos.contains(i.getSummary());
                // Clonar issue original
                if(!existeOriginal){

                    MutableIssue toclone = issueFactory.cloneIssueWithAllFields(i);

                    toclone.setProjectId(proyectoR.getId());

                    
                    try{ 
                        // Comentar para probar y que no se clonen los requisitos
                        cloned = issueManager.createIssueObject(user, toclone);
                    }
                    catch (Exception e){

                    }
                }

                    // Obtenemos los distintos tipos de enlace que tiene la issue
                    Set<IssueLinkType> typesLinks = linkCollection.getLinkTypes();
                    for(IssueLinkType t : typesLinks){
                        if(t.getName().equals("Inclusión")){
                            List<Issue> lInclusionIn = linkCollection.getInwardIssues("Inclusión");
                            if(lInclusionIn != null){
                                for(Issue il : lInclusionIn){
                                    // Comprobar que la issue del enlace no existe ya en el proyecto
                                    boolean existe = nombresRequisitos.contains(il.getSummary());
                                    if(!existe){
                                        // clonar issue del enlace
                                        MutableIssue tocloneLink = issueFactory.cloneIssueWithAllFields(il);

                                        tocloneLink.setProjectId(proyectoR.getId());

                                        Issue clonedLink = null;
                                        try{ 
                                            // Comentar para probar y que no se clonen los requisitos
                                            clonedLink = issueManager.createIssueObject(user, tocloneLink);
                                        }
                                        catch (Exception e){

                                        }
                                        // Si se va a reutilizar una issue pero tiene una dependencia que ya esta en el proyecto, 
                                        //entonces se enlaza con la que ya existe en el proyecto donde va a ser reutilizada
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(clonedLink.getId(),nombresIds.get(i.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){

                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(clonedLink.getId(),cloned.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){

                                            } 
                                        }

                                        
                                    }
                                    else{
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(il.getSummary()),nombresIds.get(i.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){

                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(il.getSummary()),cloned.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){

                                            }                                            
                                        }

                                    }
                                }

                            }
                            
                            List<Issue> lInclusionOut = linkCollection.getOutwardIssues("Inclusión");
                            if(lInclusionOut != null){
                                for(Issue il : lInclusionOut){
                                    // Comprobar que la issue del enlace no existe ya en el proyecto
                                    boolean existe = nombresRequisitos.contains(il.getSummary());
                                    if(!existe){
                                        // clonar issue del enlace
                                        MutableIssue tocloneLink = issueFactory.cloneIssueWithAllFields(il);

                                        tocloneLink.setProjectId(proyectoR.getId());

                                        Issue clonedLink = null;
                                        try{ 
                                            // Comentar para probar y que no se clonen los requisitos
                                            clonedLink = issueManager.createIssueObject(user, tocloneLink);
                                        }
                                        catch (Exception e){
                                            System.out.println("Error: " + e.getMessage());
                                        }
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(i.getSummary()), clonedLink.getId(),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                        }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(cloned.getId(), clonedLink.getId(),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            } 
                                        }

                                        
                                    }
                                    else{
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(i.getSummary()),nombresIds.get(il.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(cloned.getId(), nombresIds.get(il.getSummary()),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }                                            
                                        }

                                    }
                                }


                            }
                        }

                        if(t.getName().equals("Exclusión")){
                            List<Issue> lExclusionIn = linkCollection.getInwardIssues("Exclusión");
                            if(lExclusionIn != null){
                                for(Issue il : lExclusionIn){
                                    // Comprobar que la issue del enlace no existe ya en el proyecto
                                    boolean existe = nombresRequisitos.contains(il.getSummary());
                                    if(!existe){
                                        // clonar issue del enlace
                                        MutableIssue tocloneLink = issueFactory.cloneIssueWithAllFields(il);

                                        tocloneLink.setProjectId(proyectoR.getId());

                                        Issue clonedLink = null;
                                        try{ 
                                            // Comentar para probar y que no se clonen los requisitos
                                            clonedLink = issueManager.createIssueObject(user, tocloneLink);
                                        }
                                        catch (Exception e){
                                            System.out.println("Error: " + e.getMessage());
                                        }
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(clonedLink.getId(),nombresIds.get(i.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                        }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(clonedLink.getId(),cloned.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            } 
                                        }

                                        
                                    }
                                    else{
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(il.getSummary()),nombresIds.get(i.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(il.getSummary()),cloned.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }                                            
                                        }

                                    }
                                }

                            }
                            
                            List<Issue> lExclusionOut = linkCollection.getOutwardIssues("Exclusión");
                            if(lExclusionOut != null){
                                for(Issue il : lExclusionOut){
                                    // Comprobar que la issue del enlace no existe ya en el proyecto
                                    boolean existe = nombresRequisitos.contains(il.getSummary());
                                    if(!existe){
                                        // clonar issue del enlace
                                        MutableIssue tocloneLink = issueFactory.cloneIssueWithAllFields(il);

                                        tocloneLink.setProjectId(proyectoR.getId());

                                        Issue clonedLink = null;
                                        try{ 
                                            // Comentar para probar y que no se clonen los requisitos
                                            clonedLink = issueManager.createIssueObject(user, tocloneLink);
                                        }
                                        catch (Exception e){
                                            System.out.println("Error: " + e.getMessage());
                                        }
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(i.getSummary()), clonedLink.getId(),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                        }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(cloned.getId(),clonedLink.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            } 
                                        }

                                        
                                    }
                                    else{
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(i.getSummary()),nombresIds.get(il.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(cloned.getId(), nombresIds.get(il.getSummary()),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }                                            
                                        }

                                    }
                                }


                            }


                        }
                        if(t.getName().equals("Requiere")){
                           
                            List<Issue> lRequiereIn = linkCollection.getInwardIssues("Requiere");
                            if(lRequiereIn != null){
                                for(Issue il : lRequiereIn){
                                    // Comprobar que la issue del enlace no existe ya en el proyecto
                                    boolean existe = nombresRequisitos.contains(il.getSummary());
                                    if(!existe){
                                        // clonar issue del enlace
                                        MutableIssue tocloneLink = issueFactory.cloneIssueWithAllFields(il);

                                        tocloneLink.setProjectId(proyectoR.getId());

                                        Issue clonedLink = null;
                                        try{ 
                                            // Comentar para probar y que no se clonen los requisitos
                                            clonedLink = issueManager.createIssueObject(user, tocloneLink);
                                        }
                                        catch (Exception e){
                                            System.out.println("Error: " + e.getMessage());
                                        }
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(clonedLink.getId(),nombresIds.get(i.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                        }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(clonedLink.getId(),cloned.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            } 
                                        }

                                        
                                    }
                                    else{
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(il.getSummary()),nombresIds.get(i.getSummary()), t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(il.getSummary()),cloned.getId(), t.getId(), null, user);
                                            }
                                            catch (Exception e){

                                            }                                            
                                        }

                                    }
                                }

                            }
                            
                            List<Issue> lRequiereOut = linkCollection.getOutwardIssues("Requiere");
                            if(lRequiereOut != null){
                                for(Issue il : lRequiereOut){
                                    // Comprobar que la issue del enlace no existe ya en el proyecto
                                    boolean existe = nombresRequisitos.contains(il.getSummary());
                                    if(!existe){
                                        // clonar issue del enlace
                                        MutableIssue tocloneLink = issueFactory.cloneIssueWithAllFields(il);

                                        tocloneLink.setProjectId(proyectoR.getId());

                                        Issue clonedLink = null;
                                        try{ 
                                            // Comentar para probar y que no se clonen los requisitos
                                            clonedLink = issueManager.createIssueObject(user, tocloneLink);
                                        }
                                        catch (Exception e){
                                            System.out.println("Error: " + e.getMessage());
                                        }
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(i.getSummary()), clonedLink.getId(),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(cloned.getId(), clonedLink.getId(),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            } 
                                        }

                                        
                                    }
                                    else{
                                        if(existeOriginal){
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(nombresIds.get(i.getSummary()), nombresIds.get(il.getSummary()),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }
                                        }
                                        else{
                                            // Crear enlace
                                            try{ 
                                                issueLinkManager.createIssueLink(cloned.getId(), nombresIds.get(il.getSummary()),t.getId(), null, user);
                                            }
                                            catch (Exception e){
                                                System.out.println("Error: " + e.getMessage());
                                            }                                            
                                        }

                                    }
                                }


                            }
                        }

                    }


                    links = new ArrayList<>(clinks);
                    }
                

  

        }
                                                
        if (projectId == null) {
            return;
        }
        // Comentar si se quiere reutilizar cualquier tipo de issue
        if (type.equals("Requisito del sistema")){

        }
        else if (type.equals("Historia de usuario")){

        }
        else{
            return;
        }
 
        String cadena = "";
        try{  
            
            String aux;
            // Indicar la ubicacion local del fichero de respuesta
            File directory = new File("./");
            String[] parts = directory.getAbsolutePath().split("target");
            String ruta = parts[0]+"src"+File.separator+"main"+File.separator+"java"+File.separator+
                "com"+File.separator+"servlet"+File.separator+"respuesta.html";
            
            
            //FileReader f = new FileReader("C:\\Users\\Jose\\Desktop\\TFG\\myPluginJiraReqReuse\\src\\main\\java\\com\\servlet\\respuesta.html");
            FileReader f = new FileReader(ruta);
            BufferedReader b = new BufferedReader(f);
            while((aux = b.readLine()) != null) {
                cadena = cadena+aux;
            }
            b.close();
        }
        catch (Exception e){
            System.out.println("Error con la lectura del fichero html de respuesta: " + e.getMessage());
        }
           
            resp.setContentType("text/html");
            resp.getWriter().write(cadena);
            
            //resp.getWriter().write("<html><body>Hello World</body></html>"+nombresRequisitos.toString());
        
        
    }


}