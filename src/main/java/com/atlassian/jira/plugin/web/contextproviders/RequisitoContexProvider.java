package com.atlassian.jira.plugin.web.contextprovider;

import java.time.LocalDate;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.link.LinkCollection;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.IssueFieldConstants;

import java.util.HashMap;
import java.util.Map;
import java.util.Collection;
import java.util.ArrayList;
import java.lang.String;

public class RequisitoContexProvider extends AbstractJiraContextProvider
{
    @Override
    public Map<String, Object> getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper) {

        Map<String, Object> contextMap = new HashMap<>();

        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField selectorProyectos = customFieldManager.getCustomFieldObjectByName("Reutilizar en el proyecto");


        FieldManager fieldManager = ComponentAccessor.getComponent(FieldManager.class);
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);

        Field enlaces = fieldManager.getField(IssueFieldConstants.ISSUE_LINKS);


    	Issue currentIssue = (Issue) jiraHelper.getContextParams().get("issue");

        String key = currentIssue.getKey();

        // Comprobar si el requisito tiene el campo selectorProyectos
        if (selectorProyectos != null) {
            Object selectoProj = selectorProyectos.getValue(currentIssue);

            if (selectoProj != null){
                String[] aux = selectoProj.toString().split(" ");

                if(aux.length > 1) {
                    String proyecto = aux[1]; 
                    // Enviar a la vista el valor del proyecto donde se quiere reutiliza el requisito
                    contextMap.put("proyectoReutilizar", proyecto);
                } 
            }
        }
        

    	if(key != null){
            // Mandar a la vista la key del requisito actual
    		contextMap.put("keyCurrentIssue", key);
    	}


        if(enlaces != null){
            LinkCollection linkCollection = issueLinkManager.getLinkCollection(currentIssue, applicationUser);
            Collection<Issue> clinks = linkCollection.getAllIssues();
            // Enviar a la vista los elenaces que tiene con otros requisitos el requisito actual
            contextMap.put("enlaces", clinks);

        }

        return contextMap;
    }

}

